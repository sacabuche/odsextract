package main

import (
	"archive/zip"
	"encoding/xml"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
)

const (
	ContentFileName = "content.xml"
)

var (
	ErrNotFound = errors.New(fmt.Sprintf(`"%s" Not found`, ContentFileName))
)

type Sheet struct {
	XMLName      xml.Name `xml:"table"`
	Name         string   `xml:"name,attr"`
	Rows         []Row    `xml:"table-row"`
	total        float64
	suma_precios float64
}

type Row struct {
	Cells []Cell `xml:"table-cell"`
}

type Cell struct {
	Formula     string `xml:"formula,attr"`
	Value       string `xml:"p"`
	OfficeValue string `xml:"value,attr"`
}

type SpreadSheet struct {
	Path   string
	sheets map[string]Sheet
	Name   string
}

type ItemSell struct {
	Code        string
	Description string
	Price       float64
	DiscountPct float64
	Payed       float64
	IsFormula   bool
}

const (
	CODE         = 0
	DESCRIPTION  = 1
	PRICE        = 2
	DISCOUNT_PCT = 3
	PAYED        = 4
	INPUTS       = "Ventas"
	OUTPUTS      = "Gastos"
)

var replacerNumeric = strings.NewReplacer("$", "", "%", "")

func (r Row) Float(i int) float64 {
	value := r.Cells[i].OfficeValue
	parsed, err := strconv.ParseFloat(value, 64)
	if err != nil {
		if value == "" {
			return 0.0
		}
		log.Fatal(err)
	}
	return parsed
}

func (r Row) String(i int) string {
	return r.Cells[i].Value
}

func (r Row) IsFormula(i int) bool {
	return r.Cells[i].Formula != ""
}

func (r Row) BuildArticle() ItemSell {
	return ItemSell{
		Code:        r.String(CODE),
		Description: r.String(DESCRIPTION),
		Price:       r.Float(PRICE),
		DiscountPct: r.Float(DISCOUNT_PCT) * 100,
		Payed:       r.Float(PAYED),
		IsFormula:   r.IsFormula(PRICE),
	}
}

func (r Row) IsEmpty(i int) bool {
	if len(r.Cells) >= i+1 {
		return r.Cells[i].Value == ""
	}
	return true
}

func (sh SpreadSheet) LoadSheets() error {
	r, err := zip.OpenReader(sh.Path)
	if err != nil {
		return err
	}

	defer r.Close()
	zipfile, err := sh.getContentFile(r)
	if err != nil {
		return err
	}

	rc, err := zipfile.Open()
	if err != nil {
		return err
	}

	decoder := xml.NewDecoder(rc)
	for {
		token, _ := decoder.Token()
		if token == nil {
			break
		}

		switch startElement := token.(type) {
		case xml.StartElement:
			if startElement.Name.Local == "table" {
				var sheet Sheet
				decoder.DecodeElement(&sheet, &startElement)
				sh.sheets[sheet.Name] = sheet
			}
		}
	}

	return nil
}

func (sh SpreadSheet) getContentFile(r *zip.ReadCloser) (*zip.File, error) {
	for _, f := range r.File {
		if f.Name == ContentFileName {
			return f, nil
		}

	}
	return nil, ErrNotFound
}

func (sh SpreadSheet) ParseCells() {
	/* Este csv es un archivo temporal para verificar que los datos estan bien */
	file_name_raw := sh.Path + ".csv"
	f_raw, err := os.Create(file_name_raw)
	if err != nil {
		log.Fatal(err)
	}
	for name, s := range sh.sheets {
		io.WriteString(f_raw, "#####"+name+"#####\n")
		for _, row := range s.Rows {
			length := len(row.Cells)
			values := make([]string, length * 2)
			has_content := false
			for cell_i, cell := range row.Cells {
				var value string
				if len(cell.OfficeValue) > 0 {
					value = cell.OfficeValue
				} else {
					value = cell.Value
				}
				values[cell_i] = value
				values[length + cell_i] = fmt.Sprintf("%v", row.IsFormula(cell_i))
				if !has_content && len(value) > 0 && value != "0" {
					has_content = true
				}
			}
			if has_content {
				record := fmt.Sprintf("%s\n", strings.Join(values, "|"))
				io.WriteString(f_raw, record)
			}
		}
	}
	f_raw.Close()

	sheet := sh.sheets[INPUTS]
	for _, row := range sheet.Rows[1:] {
		if row.IsEmpty(PRICE) {
			continue
		}
		article := row.BuildArticle()
		if article.Price <= 0 {
			continue
		}

		sheet.suma_precios += article.Price
		sheet.total += article.Payed
		fmt.Println(article)
	}
	fmt.Printf("%s Totales: %f Precios: %f\n", sh.Path, sheet.total, sheet.suma_precios)
}

func main() {
	file_name := flag.String("file", "", "File to parse")
	flag.Parse()

	_, name := path.Split(*file_name)
	spreadSheet := SpreadSheet{
		Path:   *file_name,
		Name:   name,
		sheets: make(map[string]Sheet),
	}
	err := spreadSheet.LoadSheets()

	if err != nil {
		log.Fatal(err)
	}
	spreadSheet.ParseCells()
}
